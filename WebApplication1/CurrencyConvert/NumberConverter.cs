﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApplication1.CurrencyConvert
{
    public class NumberConverter
    {
        private readonly Dictionary<char, string[]> _dict = new Dictionary<char, string[]>
        {
            {'1', new[] { "One", "Eleven", "Ten" }},
            {'2', new[] { "Two", "Twelve", "Twenty" }},
            {'3', new[] { "Three", "Thirteen", "Thirty" }},
            {'4', new[] { "Four", "Fourteen", "Forty" }},
            {'5', new[] { "Five", "Fifteen", "Fifty" }},
            {'6', new[] { "Six", "Sixteen", "Sixty" }},
            {'7', new[] { "Seven", "Seventeen", "Seventy" }},
            {'8', new[] { "Eight", "Eighteen", "Eighty" }},
            {'9', new[] { "Nine", "Nineteen", "Ninety" }},
            {'0', new[] { "Zero", "Ten", "" }}
        };

        public string ErrorMessage { get; set; }

        //skips Hundred because able to have hundred thousand, etc
        private string[] _scale =
        {
            "", "Thousand", "Million", "Billion", "Trillion", "Quadrillion"
        };

        /// <summary>
        /// Converts just 1 digit; in the hundreds column and the ones column
        /// </summary>
        /// <param name="num">number to convert</param>
        /// <returns>number as word or sets error</returns>
        public string FirstDigit(char num)
        {
            string[] words;
            _dict.TryGetValue(num, out words);
            return words == null
                             ? ErrorMessage = "Invalid input: numbers only"
                             : words[0] + " ";
        }

        /// <summary>
        /// Converts the 10's column and calls FirstDigit  
        /// </summary>
        /// <param name="num">number to convert</param>
        /// <returns>number as word or sets error</returns>
        public string SecondDigit(char[] num)
        {
            string[] words;
            var str = "";

            if (num.Length < 2)
            {
                ErrorMessage = "Invalid: error occurred in code";
                return "";
            }

            if (num[0] == '0' && num[1] == '0') //nothing to convert
            {
                return "Zero ";
            }

            if (num[0] == '1') //number is 10 <= x <= 19
            {
                _dict.TryGetValue(num[1], out words);
                return words == null
                           ? ErrorMessage = "Invalid input: numbers only"
                           : words[1] + " ";
            }

            //get the 10's digit 
            if (num[0] != '0') //to skip numbers like 107
            {
                _dict.TryGetValue(num[0], out words);
                str = words == null
                          ? ErrorMessage = "Invalid input: numbers only"
                          : str + words[2] + " ";
            }

            str += FirstDigit(num[num.Length - 1]);

            return str;
        }

        /// <summary>
        /// Converts the 100's column and calls SecondDigit
        /// </summary>
        /// <param name="num">Number string to convert</param>
        /// <param name="len">Length of whole number left to get the scale (thousand, million...)</param>
        /// <returns>number as word or sets error</returns>
        public string ThirdDigit(string num, int len)
        {
            var words = "";

            var scaleIndex = (int)Math.Ceiling(len / 3.0) - 1;
            if (scaleIndex > 5)
            {
                ErrorMessage = "Invalid: number too large for this converter";
                return "";
            }

            if (num.Length != 3)
            {
                ErrorMessage = "Invalid: error occurred in code";
                return "";
            }

            if (num == "000") //nothing to convert
            {
                return "Zero ";
            }

            if (num[0] != '0')
            {
                words += FirstDigit(num[0]) + "Hundred "; //due to 100,000
            }
            words += SecondDigit(num.Substring(1).ToCharArray());

            if (scaleIndex >= 1)
            {
                words += _scale[scaleIndex] + " ";
            }
            return words;
        }

        /// <summary>
        /// Breaks the number string in chunks of 3 to convert
        /// </summary>
        /// <param name="num">Number string to convert</param>
        /// <returns>number as word or sets error</returns>
        public string ConvertNumber(string num)
        {
            var words = "";

            while (num.Length % 3 != 0) //to get into even groups of 3
            {
                num = "0" + num;
            }

            while (num.Length > 0) //deals with a group of 3 at a time
            {
                words += ThirdDigit(num.Substring(0, 3), num.Length);
                num = num.Substring(3);
            }
            return words;
        }

        /// <summary>
        /// Removes leading 0's and commas, and every number will be formatted as 0.00
        /// </summary>
        /// <param name="num">Number string to be formatted</param>
        /// <returns>Formatted number string</returns>
        public string FormatNumber(string num)
        {
            num = num.Trim();
            num = num.Replace(",", "");

            if (num.Length == 0)
            {
                num = "0";
            }
            if (num.Length > 1 && num[0] == '$') //remove first $, any more will & should cause error
            {
                num = num.Substring(1);
            }
            if (num.IndexOf('.') != num.LastIndexOf('.'))
            {
                ErrorMessage = "Invalid number: too many decimal points";
                return "";
            }

            while (num[0] == '0') // remove leading 0's
            {
                if (num.Length > 1)
                {
                    num = num.Substring(1);
                }
                else //input was 0's
                {
                    break;
                }
            }
            if (num.Contains('.'))
            {
                if (num[0] == '.') //need to add leading 0
                {
                    num = "0" + num;
                }
                if (num.Last() == '.') //need to add trailing 0's
                {
                    num += "00";
                }
                return num;
            }
            return num + ".00";
        }

        /// <summary>
        /// Calls the conversion and then formats string
        /// </summary>
        /// <param name="number">Number string to be formatted</param>
        /// <returns>Formatted number string</returns>
        public string StartConversion(string number)
        {
            var wordStr = "";
            if (number.Length > 1 && number[0] == '-') //remove first -, any more will & should cause error
            {
                number = number.Substring(1);
                wordStr += "Negative ";
            }
            number = FormatNumber(number);

            if (!string.IsNullOrWhiteSpace(ErrorMessage))
            {
                return "";
            }

            var splitNumber = number.Split('.');
            var dollars = ConvertNumber(splitNumber[0]);
            var cents = SecondDigit((splitNumber[1].Length < 2
                                         ? splitNumber[1] + "0"
                                         : splitNumber[1]).ToCharArray());

            if (dollars.Trim() != "Zero") //to remove any unwanted 'Zero'
            {
                dollars = dollars.Replace("Zero ", "");
            }
            if (cents.Trim() != "Zero") //to remove any unwanted 'Zero'
            {
                cents = cents.Replace("Zero ", "");
            }

            if (!string.IsNullOrWhiteSpace(dollars + cents))
            {
                wordStr += dollars + (dollars.Trim() == "One"
                                          ? "Dollar "
                                          : "Dollars ");
                wordStr += "And ";
                wordStr += cents + (cents.Trim() == "One"
                                        ? "Cent"
                                        : "Cents");
                return wordStr;
            }
            return "";
        }
    }
}