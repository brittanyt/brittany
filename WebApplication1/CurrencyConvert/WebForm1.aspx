﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.CurrencyConvert.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Number Conversion</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin: 20px;">
            <div style="font-size: 16pt; margin-bottom: 20px;">
                Input number to convert to an English word currency representation.
            </div>
            <asp:TextBox ID="inNumber" Width="300px" runat="server" />
            <asp:Button ID="btnGo" runat="server" OnClick="btnButton_Click" Text="Convert" />
            <div style="margin-top: 20px;">
                <label id="lblOutput" runat="server"></label>
            </div>
        </div>
    </form>
</body>
</html>
