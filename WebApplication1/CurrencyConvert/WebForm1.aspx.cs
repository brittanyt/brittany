﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApplication1.CurrencyConvert
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void btnButton_Click(object sender, EventArgs e)
        {
            var nc = new NumberConverter
            {
                ErrorMessage = null
            };

            var number = inNumber.Text;
            if (string.IsNullOrWhiteSpace(number))
            {
                lblOutput.InnerText = "Please enter a number";
                return;
            }

            var result = nc.StartConversion(number);
            if (!string.IsNullOrWhiteSpace(nc.ErrorMessage))
            {
                lblOutput.InnerText = nc.ErrorMessage;
                return;
            }
            lblOutput.InnerText = result;
        }
    }
}