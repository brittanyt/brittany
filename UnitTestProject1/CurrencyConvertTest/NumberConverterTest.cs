﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplication1.CurrencyConvert;

namespace UnitTestProject1
{
    [TestClass]
    public class NumberConverterTest
    {
        public const int ASCII_MAX = 127;

        [TestMethod]
        public void TestFirstDigit()
        {
            var nc = new NumberConverter();

            for (var num = (char)0; num <= ASCII_MAX; num++)
            {
                nc.ErrorMessage = null;
                var actual = nc.FirstDigit(num);
                switch (num)
                {
                    case '1':
                        Assert.AreEqual(actual, "One ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '2':
                        Assert.AreEqual(actual, "Two ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '3':
                        Assert.AreEqual(actual, "Three ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '4':
                        Assert.AreEqual(actual, "Four ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '5':
                        Assert.AreEqual(actual, "Five ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '6':
                        Assert.AreEqual(actual, "Six ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '7':
                        Assert.AreEqual(actual, "Seven ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '8':
                        Assert.AreEqual(actual, "Eight ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '9':
                        Assert.AreEqual(actual, "Nine ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    case '0':
                        Assert.AreEqual(actual, "Zero ");
                        Assert.AreEqual(nc.ErrorMessage, null);
                        break;
                    default:
                        Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
                        break;
                }
            }
        }

        [TestMethod]
        public void TestSecondDigit()
        {
            var nc = new NumberConverter
            {
                ErrorMessage = null
            };
            var actual = nc.SecondDigit("10".ToCharArray());
            Assert.AreEqual(actual, "Ten ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("11".ToCharArray());
            Assert.AreEqual(actual, "Eleven ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("12".ToCharArray());
            Assert.AreEqual(actual, "Twelve ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("13".ToCharArray());
            Assert.AreEqual(actual, "Thirteen ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("19".ToCharArray());
            Assert.AreEqual(actual, "Nineteen ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.SecondDigit("20".ToCharArray());
            Assert.AreEqual(actual, "Twenty Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("21".ToCharArray());
            Assert.AreEqual(actual, "Twenty One ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("90".ToCharArray());
            Assert.AreEqual(actual, "Ninety Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("99".ToCharArray());
            Assert.AreEqual(actual, "Ninety Nine ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.SecondDigit("00".ToCharArray());
            Assert.AreEqual(actual, "Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("01".ToCharArray());
            Assert.AreEqual(actual, "One ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.SecondDigit("07".ToCharArray());
            Assert.AreEqual(actual, "Seven ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            nc.SecondDigit("".ToCharArray());
            Assert.AreEqual(nc.ErrorMessage, "Invalid: error occurred in code");
            nc.ErrorMessage = null;
            nc.SecondDigit("0".ToCharArray());
            Assert.AreEqual(nc.ErrorMessage, "Invalid: error occurred in code");
            nc.ErrorMessage = null;
            nc.SecondDigit("a1".ToCharArray());
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
        }

        [TestMethod]
        public void TestThirdDigit()
        {
            var nc = new NumberConverter
            {
                ErrorMessage = null
            };
            var actual = nc.ThirdDigit("100", 3);
            Assert.AreEqual(actual, "One Hundred Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("101", 3);
            Assert.AreEqual(actual, "One Hundred One ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("110", 3);
            Assert.AreEqual(actual, "One Hundred Ten ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("111", 3);
            Assert.AreEqual(actual, "One Hundred Eleven ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("112", 3);
            Assert.AreEqual(actual, "One Hundred Twelve ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("120", 3);
            Assert.AreEqual(actual, "One Hundred Twenty Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("121", 3);
            Assert.AreEqual(actual, "One Hundred Twenty One ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("921", 3);
            Assert.AreEqual(actual, "Nine Hundred Twenty One ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("000", 3);
            Assert.AreEqual(actual, "Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("000", 13);
            Assert.AreEqual(actual, "Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("001", 4);
            Assert.AreEqual(actual, "One Thousand ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("100", 4);
            Assert.AreEqual(actual, "One Hundred Zero Thousand ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ThirdDigit("110", 4);
            Assert.AreEqual(actual, "One Hundred Ten Thousand ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            nc.ThirdDigit("921", 18);
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            nc.ThirdDigit("921", 19);
            Assert.AreEqual(nc.ErrorMessage, "Invalid: number too large for this converter");
            nc.ErrorMessage = null;
            nc.ThirdDigit("a11", 19);
            Assert.AreEqual(nc.ErrorMessage, "Invalid: number too large for this converter");

            nc.ErrorMessage = null;
            nc.ThirdDigit("a11", 5);
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
            nc.ErrorMessage = null;
            nc.ThirdDigit("1a1", 5);
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
            nc.ErrorMessage = null;
            nc.ThirdDigit("11a", 5);
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
            nc.ErrorMessage = null;
            nc.ThirdDigit("1.1", 5);
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
            nc.ErrorMessage = null;
            nc.ThirdDigit("1,1", 5);
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");

            nc.ErrorMessage = null;
            nc.ThirdDigit("10", 2);
            Assert.AreEqual(nc.ErrorMessage, "Invalid: error occurred in code");
            nc.ErrorMessage = null;
            nc.ThirdDigit("0", 1);
            Assert.AreEqual(nc.ErrorMessage, "Invalid: error occurred in code");
        }


        [TestMethod]
        public void TestConvertNumber()
        {
            var nc = new NumberConverter
            {
                ErrorMessage = null
            };
            var actual = nc.ConvertNumber("100");
            Assert.AreEqual(actual, "One Hundred Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("101");
            Assert.AreEqual(actual, "One Hundred One ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("110");
            Assert.AreEqual(actual, "One Hundred Ten ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("111");
            Assert.AreEqual(actual, "One Hundred Eleven ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("112");
            Assert.AreEqual(actual, "One Hundred Twelve ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("120");
            Assert.AreEqual(actual, "One Hundred Twenty Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("121");
            Assert.AreEqual(actual, "One Hundred Twenty One ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("921");
            Assert.AreEqual(actual, "Nine Hundred Twenty One ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("000");
            Assert.AreEqual(actual, "Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("000");
            Assert.AreEqual(actual, "Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("1");
            Assert.AreEqual(actual, "One ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("0");
            Assert.AreEqual(actual, "Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("10");
            Assert.AreEqual(actual, "Ten ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("11");
            Assert.AreEqual(actual, "Eleven ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("0000010");
            Assert.AreEqual(actual, "Zero Zero Ten ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("0011");
            Assert.AreEqual(actual, "Zero Eleven ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("1000");
            Assert.AreEqual(actual, "One Thousand Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("001000");
            Assert.AreEqual(actual, "One Thousand Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("100000");
            Assert.AreEqual(actual, "One Hundred Zero Thousand Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.ConvertNumber("110000");
            Assert.AreEqual(actual, "One Hundred Ten Thousand Zero ");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            nc.ConvertNumber("921000009850136745");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            nc.ConvertNumber("9210000098501367451");
            Assert.AreEqual(nc.ErrorMessage, "Invalid: number too large for this converter");

            nc.ErrorMessage = null;
            nc.ConvertNumber("11a");
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
            nc.ErrorMessage = null;
            nc.ConvertNumber("1.10");
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
            nc.ErrorMessage = null;
            nc.ConvertNumber("1,100");
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
        }

        [TestMethod]
        public void TestFormatNumber()
        {
            var nc = new NumberConverter
            {
                ErrorMessage = null
            };
            var actual = nc.FormatNumber("100");
            Assert.AreEqual(actual, "100.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber("1");
            Assert.AreEqual(actual, "1.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber("0");
            Assert.AreEqual(actual, "0.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber("010");
            Assert.AreEqual(actual, "10.00");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.FormatNumber("");
            Assert.AreEqual(actual, "0.00");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.FormatNumber("1,000");
            Assert.AreEqual(actual, "1000.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber("1,,00");
            Assert.AreEqual(actual, "100.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber(",,");
            Assert.AreEqual(actual, "0.00");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.FormatNumber("a");
            Assert.AreEqual(actual, "a.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber("a.");
            Assert.AreEqual(actual, "a.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber(".1");
            Assert.AreEqual(actual, "0.1");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber(".0");
            Assert.AreEqual(actual, "0.0");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber("1.");
            Assert.AreEqual(actual, "1.00");
            Assert.AreEqual(nc.ErrorMessage, null);
            nc.ErrorMessage = null;
            actual = nc.FormatNumber("1.0015");
            Assert.AreEqual(actual, "1.0015");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            nc.FormatNumber("1.0.0");
            Assert.AreEqual(nc.ErrorMessage, "Invalid number: too many decimal points");
            nc.ErrorMessage = null;
            nc.FormatNumber("1..00");
            Assert.AreEqual(nc.ErrorMessage, "Invalid number: too many decimal points");
            nc.ErrorMessage = null;
            nc.FormatNumber(".1.00");
            Assert.AreEqual(nc.ErrorMessage, "Invalid number: too many decimal points");
            nc.ErrorMessage = null;
            nc.FormatNumber("..100");
            Assert.AreEqual(nc.ErrorMessage, "Invalid number: too many decimal points");
            nc.ErrorMessage = null;
            nc.FormatNumber("1..");
            Assert.AreEqual(nc.ErrorMessage, "Invalid number: too many decimal points");
            nc.ErrorMessage = null;
            nc.FormatNumber("..");
            Assert.AreEqual(nc.ErrorMessage, "Invalid number: too many decimal points");
        }

        [TestMethod]
        public void TestStartConversion()
        {
            var nc = new NumberConverter
            {
                ErrorMessage = null
            };
            var actual = nc.StartConversion("100");
            Assert.AreEqual(actual, "One Hundred Dollars And Zero Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("1");
            Assert.AreEqual(actual, "One Dollar And Zero Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("1.1");
            Assert.AreEqual(actual, "One Dollar And Ten Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("1.01");
            Assert.AreEqual(actual, "One Dollar And One Cent");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("$1.01");
            Assert.AreEqual(actual, "One Dollar And One Cent");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("-$1.01");
            Assert.AreEqual(actual, "Negative One Dollar And One Cent");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("-1.01");
            Assert.AreEqual(actual, "Negative One Dollar And One Cent");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("100,010");
            Assert.AreEqual(actual, "One Hundred Thousand Ten Dollars And Zero Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("10,0,010.1,");
            Assert.AreEqual(actual, "One Hundred Thousand Ten Dollars And Ten Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("10,100,010.15");
            Assert.AreEqual(actual, "Ten Million One Hundred Thousand Ten Dollars And Fifteen Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion("");
            Assert.AreEqual(actual, "Zero Dollars And Zero Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            actual = nc.StartConversion(",0");
            Assert.AreEqual(actual, "Zero Dollars And Zero Cents");
            Assert.AreEqual(nc.ErrorMessage, null);

            nc.ErrorMessage = null;
            nc.StartConversion("a");
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");

            nc.ErrorMessage = null;
            nc.StartConversion("$-1");
            Assert.AreEqual(nc.ErrorMessage, "Invalid input: numbers only");
        }
    }
}
